*** Settings ***
Documentation     This resource file contains common and global variables
Library           OperatingSystem
Library           String    
Library           Screenshot
Library           SeleniumLibrary
Library           Collections

*** Keywords ***
AD: Login To App
    [Documentation]    Authentication method for logging the application with a specific user information. ${\n}
    [Arguments]    ${UserName}    ${UserPassword}
    Open Browser    ${AppUrl}    headlessfirefox
    Log    ${\n} open the user url ${AppUrl} on the browser ${browser}    console=yes
    Maximize Browser Window
    Log    Test step : setting the user Name ${UserName}    console=yes
    Wait Until Page Contains Element    //*[@id="username"]
    Input Text    name=username    ${UserName}
    Log    user Name ${UserName}    console=yes
    Input Password    name=password    ${UserPassword}
    Log    user password ${UserPassword}    console=yes
    Click Element   xpath=//*[@id="login"]/button/i
    Log    click login    console=yes




