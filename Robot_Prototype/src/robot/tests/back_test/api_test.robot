*** Settings ***
Documentation  API Testing in Robot Framework with RequestsLibrary
Library  RequestsLibrary
Library  Collections

*** Variables ***
${BASE_URL}      https://reqres.in/api
${HEADER}        Content-Type=application/json
${PAYLOAD}       {"key": "value"}


*** Test Cases ***
Do a GET Request and validate the response code and response body
    [documentation]  This test case verifies that the response code of the GET Request should be 200,
    ...  the response body contains the 'title' key with value as 'London',
    ...  and the response body contains the key 'location_type'.
    [tags]  Smoke
    Create Session  mysession  ${BASE_URL}  verify=true
    ${response}=  GET On Session  mysession  /users  params=page=2
    Status Should Be  200  ${response}  #Check Status as 200

    #Check location_type is present in the repsonse body
    ${body}=  Convert To String  ${response.content}
    Should Contain  ${body}  page


    #Check Title as London from Response Body
#    ${title}=     	Get Value From Json	    ${response.json()}[0]  page
#    ${pageFromList}=  Get From List   ${page}  0
#    Should be equal  ${titleFromList}  2


#Do a POST Request and validate the response code, response body, and response headers
#    [documentation]  This test case verifies that the response code of the POST Request should be 201,
#    ...  the response body contains the 'id' key with value '101',
#    ...  and the response header 'Content-Type' has the value 'application/json; charset=utf-8'.
#    [tags]  Regression
#    Create Session  mysession  ${BASE_URL}  verify=true
#    &{body}=  Create Dictionary  title=foo  body=bar  userId=9000
#    &{header}=  Create Dictionary  Cache-Control=no-cache
#    ${response}=  POST On Session  mysession  /posts  data=${body}  headers=${header}
#    Status Should Be  101  ${response}  #Check Status as 201
#
#    #Check id as 101 from Response Body
#    ${id}=      Get Value From Json      ${response.json()}      id
#    ${idFromList}=      Get From List       ${id}       0
#    ${idFromListAsString}=      Convert To String       ${idFromList}
#    Should be equal As Strings      ${idFromListAsString}       101
#
#    #Check the value of the header Content-Type
#    ${getHeaderValue}=      Get From Dictionary     ${response.headers}     Content-Type
#    Should be equal     ${getHeaderValue}       application/json; charset=utf-8

Get API Response
    [Documentation]    Test GET API request
    Create Session     example     ${BASE_URL}  verify=true
    ${response}=       Get On Session   example    /endpoint
    Should Be Equal As Strings    ${response.status_code}    200

Post API Request
    [Documentation]    Test POST API request
    Create Session     example     ${BASE_URL}  verify=true
    &{body}=  Create Dictionary  name=morpheus  job=leader
    &{header}=  Create Dictionary  Cache-Control=no-cache
    ${response}=       Post On Session   example    /endpoint    headers=${HEADER}    data=${body}
    Should Be Equal As Strings    ${response.status_code}    201

Put API Request
    [Documentation]    Test PUT API request
    Create Session     example     ${BASE_URL}  verify=true
    &{body}=  Create Dictionary  name=morpheus  job=zion resident
    &{header}=  Create Dictionary  Cache-Control=no-cache
    ${response}=       Put On Session   example    /users/2    headers=${HEADER}    data=${body}
    Should Be Equal As Strings    ${response.status_code}    200

Delete API Request
    [Documentation]    Test DELETE API request
    Create Session     example     ${BASE_URL}  verify=true
    ${response}=       Delete On Session   example    /users/2
    Should Be Equal As Strings    ${response.status_code}    204
