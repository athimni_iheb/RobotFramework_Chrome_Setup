*** Settings ***
Documentation     login test script prototype.
Library           String
Library           DateTime
Resource          ../../ressources/General_Keyword.robot
Resource          ../../../../Configuration/conf_template.robot



*** Test Cases ***
Testcase: Authentification_Prototype
    [Documentation]    Authentification Page Test. ${\n}
    [Tags]    login_test
    AD: Login To App    ${UserName}    ${UserPassword}
    Wait Until Page Contains Element    xpath=//*[@id="flash"]    15s
    Wait Until Page Contains Element    xpath=//*[@id="content"]/div/h4    15s

    ${Welcome_Message}=        Get Text    //*[@id="content"]/div/h4
    log    ${Welcome_Message.strip()}
    Set Test Variable    ${the_Welcome_Message}    'Welcome to the Secure Area. When you are done click logout below.'
    Should Be Equal    '${Welcome_Message.strip()}'    ${the_Welcome_Message}
#    ${PopUp_Message}=        Get Text    //*[@id="flash"]/text()
#    log    ${PopUp_Message.strip()}
#    Set Test Variable    ${the_PopUp_Message}    'You logged into a secure area!'
#    Should Be Equal    '${PopUp_Message.strip()}'    ${the_PopUp_Message}

    ${RN}=    Generate Random String    4    [NUMBERS][UPPER]
    Set Test Variable    ${nomCodeArticle}    ArticleForApiStartTest_${RN}
    Log    Test step Random String : ${RN}    console=yes
